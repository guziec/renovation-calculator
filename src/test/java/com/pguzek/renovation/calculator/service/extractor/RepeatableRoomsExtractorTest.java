package com.pguzek.renovation.calculator.service.extractor;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class RepeatableRoomsExtractorTest {

    private RepeatableRoomsExtractor sut;

    @BeforeEach
    void init() {
        sut = new RepeatableRoomsExtractor();
    }

    @DisplayName("Should correctly resolve repeatable object and list them in random order")
    @ParameterizedTest
    @MethodSource("provideRoomsData")
    void getRepeatableRooms(List<Room> givenRooms, Set<Room> expectedRooms) {

        //when
        Set<Room> result = sut.getRepeatableRooms(givenRooms);

        //expect
        assertThat(result, is(expectedRooms));
    }

    private static Stream<Arguments> provideRoomsData() {
        return Stream.of(
                Arguments.of(List.of(
                        new Room(3, 4, 5, Unit.FEET),
                        new Room(1, 2, 3, Unit.FEET),
                        new Room(4, 3, 5, Unit.FEET),
                        new Room(3, 4, 5, Unit.FEET),
                        new Room(3, 4, 5, Unit.FEET)
                ), Set.of(new Room(3, 4, 5, Unit.FEET))),

                Arguments.of(List.of(
                        new Room(3, 4, 5, Unit.FEET),
                        new Room(1, 2, 3, Unit.FEET),
                        new Room(1, 1, 1, Unit.FEET),
                        new Room(1, 1, 1, Unit.FEET),
                        new Room(3, 4, 5, Unit.FEET)
                ), Set.of(new Room(3, 4, 5, Unit.FEET),
                        new Room(1, 1, 1, Unit.FEET))),

                Arguments.of(List.of(
                        new Room(3, 4, 5, Unit.FEET),
                        new Room(1, 2, 3, Unit.FEET),
                        new Room(1, 1, 1, Unit.FEET)
                ), Set.of())
        );
    }
}