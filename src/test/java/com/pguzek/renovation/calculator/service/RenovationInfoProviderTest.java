package com.pguzek.renovation.calculator.service;

import com.pguzek.renovation.calculator.file.reader.FileReader;
import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import com.pguzek.renovation.calculator.service.calculator.WallpaperAmountCalculatorStrategy;
import com.pguzek.renovation.calculator.service.extractor.CubicalRoomsExtractor;
import com.pguzek.renovation.calculator.service.extractor.RepeatableRoomsExtractor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.mockito.Mockito.*;

class RenovationInfoProviderTest {

    private RenovationInfoProvider sut;
    private FileReader fileReader;
    private WallpaperAmountCalculatorStrategy strategy;
    private RepeatableRoomsExtractor repeatableRoomsExtractor;
    private CubicalRoomsExtractor cubicalRoomsExtractor;

    @BeforeEach
    void init() {
        fileReader = mock(FileReader.class);
        strategy = mock(WallpaperAmountCalculatorStrategy.class);
        repeatableRoomsExtractor = mock(RepeatableRoomsExtractor.class);
        cubicalRoomsExtractor = mock(CubicalRoomsExtractor.class);
        sut = new RenovationInfoProvider(fileReader, strategy, repeatableRoomsExtractor, cubicalRoomsExtractor);
    }

    @Test
    void testServicesInvocations() {
        //given
        String url = "url";
        Unit unit = Unit.FEET;
        List<Room> rooms = List.of(new Room(1, 2, 3, Unit.FEET));

        when(fileReader.readFile(url, unit)).thenReturn(rooms);

        //when
        sut.getRenovationInfo(url, unit);

        //then
        verify(fileReader, times(1)).readFile(url, unit);
        verify(repeatableRoomsExtractor, times(1)).getRepeatableRooms(rooms);
        verify(strategy, times(1)).getTotalAmountOfWallpaper(rooms);
        verify(cubicalRoomsExtractor, times(1)).getCubicalRooms(rooms);
    }
}