package com.pguzek.renovation.calculator.service.calculator;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import com.pguzek.renovation.calculator.model.WallpaperAmount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

class RectangularRoomWithBufferCalculatorTest {

    private RectangularRoomWithBufferCalculator sut;

    @BeforeEach
    void init() {
        sut = new RectangularRoomWithBufferCalculator();
    }

    @DisplayName("Should calculate amount of wallpaper needed for all rooms in given List")
    @ParameterizedTest
    @MethodSource("provideData")
    void shouldCalculateTotalNeededWallpaper(List<Room> rooms, WallpaperAmount expected) {

        //when
        WallpaperAmount result = sut.getTotalAmountOfWallpaper(rooms);

        //expect
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideData() {
        return Stream.of(
                Arguments.of(List.of(
                        new Room(3, 2, 1, Unit.FEET)
                ), new WallpaperAmount(BigDecimal.valueOf(24.0), Unit.SQUARE_FEET)),

                Arguments.of(List.of(
                        new Room(1, 1, 5, Unit.FEET)
                ), new WallpaperAmount(BigDecimal.valueOf(23.0), Unit.SQUARE_FEET)),


                Arguments.of(List.of(
                        new Room(2.6d, 3.0, 10.29d, Unit.FEET)
                ), new WallpaperAmount(BigDecimal.valueOf(138.6), Unit.SQUARE_FEET)),


                Arguments.of(List.of(), new WallpaperAmount(BigDecimal.ZERO, null)),

                Arguments.of(List.of(
                        new Room(1, 1, 5, Unit.FEET),
                        new Room(1, 2, 3, Unit.FEET)
                ), new WallpaperAmount(BigDecimal.valueOf(47.0), Unit.SQUARE_FEET))
        );
    }

}