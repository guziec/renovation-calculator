package com.pguzek.renovation.calculator.service.extractor;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

class CubicalRoomsExtractorTest {

    private CubicalRoomsExtractor sut;

    @BeforeEach
    void init() {
        sut = new CubicalRoomsExtractor();
    }

    @DisplayName("Should extract Cubical-shaped rooms and present them descending")
    @ParameterizedTest
    @MethodSource("provideRoomsData")
    void extractCubicalRooms(List<Room> givenRooms, List<Room> expectedRooms) {
        //when
        List<Room> result = sut.getCubicalRooms(givenRooms);

        //expect
        Assertions.assertEquals(expectedRooms, result);
    }

    private static Stream<Arguments> provideRoomsData() {
        return Stream.of(
                Arguments.of(List.of(
                        new Room(3, 3, 3, Unit.FEET),
                        new Room(5, 5, 5, Unit.FEET),
                        new Room(1, 1, 1, Unit.FEET),
                        new Room(3, 4, 5, Unit.FEET),
                        new Room(2, 4, 7, Unit.FEET)
                        ), List.of(
                        new Room(5, 5, 5, Unit.FEET),
                        new Room(3, 3, 3, Unit.FEET),
                        new Room(1, 1, 1, Unit.FEET)
                        )
                ),

                Arguments.of(List.of(
                        new Room(1, 4, 5, Unit.FEET),
                        new Room(2, 2, 3, Unit.FEET),
                        new Room(1, 3, 1, Unit.FEET),
                        new Room(2, 1, 1, Unit.FEET),
                        new Room(3, 4, 5, Unit.FEET)
                ), List.of()),

                Arguments.of(List.of(), List.of())
        );
    }
}