package com.pguzek.renovation.calculator.file.reader.parser;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class StandardizedDimensionLineParserTest {

    private StandardizedDimensionLineParser sut;

    @BeforeEach
    void init() {
        sut = new StandardizedDimensionLineParser();
    }


    @DisplayName("Should parse given line to expected Room type")
    @ParameterizedTest
    @MethodSource("provideCorrectLineParserInput")
    void testLineParsing(String line, Room expectedRoom) {
        //given
        Unit unit = Unit.FEET;

        //when
        Room result = sut.parseLine(line, unit);

        //then
        Assertions.assertEquals(expectedRoom, result);
    }

    @DisplayName("Should throw IllegalArgumentException when data is incorrect")
    @ParameterizedTest
    @MethodSource("provideErroringInputData")
    void shouldThrowExceptionIfInputIsInvalid(String inputLine) {
        //given
        Unit unit = Unit.FEET;

        //expect
        Assertions.assertThrows(IllegalArgumentException.class, () -> sut.parseLine(inputLine, unit));
    }

    private static Stream<Arguments> provideCorrectLineParserInput() {
        return Stream.of(
                Arguments.of("1x2x3", new Room(1.0, 2.0, 3.0, Unit.FEET)),
                Arguments.of("999x999x999", new Room(999.0, 999.0, 999.0, Unit.FEET)),
                Arguments.of("90.12x12.31x9", new Room(90.12, 12.31, 9.0, Unit.FEET))
        );
    }

    private static Stream<Arguments> provideErroringInputData() {
        return Stream.of(
                Arguments.of("1-2-3"),
                Arguments.of("999x999"),
                Arguments.of("90.12x12.31x9X13")
        );
    }

}