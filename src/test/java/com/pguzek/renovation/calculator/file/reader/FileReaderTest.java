package com.pguzek.renovation.calculator.file.reader;

import com.pguzek.renovation.calculator.file.reader.parser.StandardizedDimensionLineParser;
import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FileReaderTest {

    private FileReader sut;
    private StandardizedDimensionLineParser lineParser;

    @BeforeEach
    void init() {
        lineParser = mock(StandardizedDimensionLineParser.class);
        sut = new FileReader(lineParser);
    }

    @Test
    void testLoadingFile() {
        //given
        String filePath = "files/testInput.txt";
        Unit unit = Unit.FEET;
        Room expectedRoom = new Room(1, 2, 3, unit);
        List<Room> expectedOutput = List.of(expectedRoom, expectedRoom, expectedRoom);
        when(lineParser.parseLine(any(), any())).thenReturn(expectedRoom);

        //when
        List<Room> result = sut.readFile(filePath, Unit.FEET);

        //then
        Assertions.assertEquals(expectedOutput, result);
    }
}