package com.pguzek.renovation.calculator;

import com.pguzek.renovation.calculator.file.reader.FileReader;
import com.pguzek.renovation.calculator.file.reader.parser.StandardizedDimensionLineParser;
import com.pguzek.renovation.calculator.model.RenovationInfo;
import com.pguzek.renovation.calculator.model.Unit;
import com.pguzek.renovation.calculator.service.RenovationInfoProvider;
import com.pguzek.renovation.calculator.service.calculator.RectangularRoomWithBufferCalculator;
import com.pguzek.renovation.calculator.service.extractor.CubicalRoomsExtractor;
import com.pguzek.renovation.calculator.service.extractor.RepeatableRoomsExtractor;

public class Main {
    private static final String INPUT_DATA_PATH = "files/input1.txt";

    public static void main(String[] args) {

        /* Instead of initializing RenovationInfoProvider here and passing parameters,
        maybe it would be better to provide static method in new interface and do sort of Dependency Injection there
         */

        RenovationInfoProvider renovationInfoProvider = new RenovationInfoProvider(
                new FileReader(new StandardizedDimensionLineParser()), new RectangularRoomWithBufferCalculator(),
                new RepeatableRoomsExtractor(), new CubicalRoomsExtractor()
        );
        RenovationInfo renovationInfo = renovationInfoProvider.getRenovationInfo(INPUT_DATA_PATH, Unit.FEET);

        System.out.println("Total wallpaper needed: " + renovationInfo.getTotalAmountWallpaperNeeded() + "\n");
        System.out.println("Rooms with cubic shape: ");
        renovationInfo.getCubicShapedRooms().forEach(System.out::println);

        System.out.println("\nRooms which are appearing more then once:");
        renovationInfo.getRepetableRooms().forEach(System.out::println);

    }
}
