package com.pguzek.renovation.calculator.service.calculator;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.WallpaperAmount;

import java.util.List;

public interface WallpaperAmountCalculatorStrategy {

    WallpaperAmount getTotalAmountOfWallpaper(List<Room> rooms);

}
