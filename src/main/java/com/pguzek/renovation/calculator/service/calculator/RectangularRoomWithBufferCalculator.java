package com.pguzek.renovation.calculator.service.calculator;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import com.pguzek.renovation.calculator.model.WallpaperAmount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

public class RectangularRoomWithBufferCalculator implements WallpaperAmountCalculatorStrategy {
    private static final Logger log = LoggerFactory.getLogger(RectangularRoomWithBufferCalculator.class);

    @Override
    public WallpaperAmount getTotalAmountOfWallpaper(List<Room> rooms) {
        log.debug("Calculating total number of wallpaper with RectangularRoomWithBufferCalculator");
        BigDecimal wallpaperAmount = rooms.stream()
                .map(this::calculateWallpaperAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        Unit unit = rooms.isEmpty() ? Unit.UNKNOWN : rooms.get(0).getUnit();
        return new WallpaperAmount(wallpaperAmount, unit.getSquareValue());
    }

    private BigDecimal calculateWallpaperAmount(Room room) {
        return BigDecimal.valueOf(2 * room.getLength() * room.getWidth() +
                2 * room.getWidth() * room.getHeight() +
                2 * room.getHeight() * room.getLength() +
                addBufferForSmallestSide(room))
                .setScale(1, RoundingMode.HALF_UP);
    }

    private double addBufferForSmallestSide(Room room) {
        List<Double> roomDimensions = sortDimensionsAsc(room);
        Double firstDimension = roomDimensions.get(0);
        Double secondDimension = roomDimensions.get(1);
        return firstDimension * secondDimension;
    }

    private List<Double> sortDimensionsAsc(Room room) {
        return List.of(room.getHeight(), room.getLength(), room.getWidth())
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }
}