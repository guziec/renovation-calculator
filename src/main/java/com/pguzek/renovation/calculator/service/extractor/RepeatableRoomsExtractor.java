package com.pguzek.renovation.calculator.service.extractor;

import com.pguzek.renovation.calculator.model.Room;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RepeatableRoomsExtractor {

    public Set<Room> getRepeatableRooms(List<Room> rooms) {

        Set<Room> repetableRooms = new HashSet<>();
        Set<Room> roomSet = new HashSet<>();
        for (Room room : rooms) {
            if (!roomSet.add(room)) {
                repetableRooms.add(room);
            }
        }
        return repetableRooms;
    }
}
