package com.pguzek.renovation.calculator.service;

import com.pguzek.renovation.calculator.file.reader.FileReader;
import com.pguzek.renovation.calculator.model.RenovationInfo;
import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import com.pguzek.renovation.calculator.service.calculator.WallpaperAmountCalculatorStrategy;
import com.pguzek.renovation.calculator.service.extractor.CubicalRoomsExtractor;
import com.pguzek.renovation.calculator.service.extractor.RepeatableRoomsExtractor;

import java.util.List;

public class RenovationInfoProvider {

    private final FileReader fileReader;
    private final WallpaperAmountCalculatorStrategy strategy;
    private final RepeatableRoomsExtractor repeatableRoomsExtractor;
    private final CubicalRoomsExtractor cubicalRoomsExtractor;

    public RenovationInfoProvider(FileReader fileReader, WallpaperAmountCalculatorStrategy strategy,
                                  RepeatableRoomsExtractor repeatableRoomsExtractor,
                                  CubicalRoomsExtractor cubicalRoomsExtractor) {
        this.fileReader = fileReader;
        this.strategy = strategy;
        this.repeatableRoomsExtractor = repeatableRoomsExtractor;
        this.cubicalRoomsExtractor = cubicalRoomsExtractor;
    }

    public RenovationInfo getRenovationInfo(String file, Unit dimensionUnit) {
        List<Room> rooms = fileReader.readFile(file, dimensionUnit);

        return new RenovationInfo(
                strategy.getTotalAmountOfWallpaper(rooms),
                cubicalRoomsExtractor.getCubicalRooms(rooms),
                repeatableRoomsExtractor.getRepeatableRooms(rooms)
        );
    }
}
