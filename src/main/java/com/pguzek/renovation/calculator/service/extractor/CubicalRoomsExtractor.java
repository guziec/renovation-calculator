package com.pguzek.renovation.calculator.service.extractor;

import com.pguzek.renovation.calculator.model.Room;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CubicalRoomsExtractor {

    public List<Room> getCubicalRooms(List<Room> rooms) {
        return rooms.stream()
                .filter(Room::isCubicShape)
                .sorted(Comparator.comparingDouble(Room::getHeight).reversed())
                .collect(Collectors.toList());
    }
}
