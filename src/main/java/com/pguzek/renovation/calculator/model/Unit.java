package com.pguzek.renovation.calculator.model;

public enum Unit {

    UNKNOWN(null),
    SQUARE_FEET(null),
    FEET(SQUARE_FEET);

    private Unit squareValue;

    Unit(Unit squareValue) {
        this.squareValue = squareValue;
    }

    public Unit getSquareValue() {
        return squareValue;
    }
}
