package com.pguzek.renovation.calculator.model;

import java.math.BigDecimal;
import java.util.Objects;

public class WallpaperAmount {
    private final BigDecimal amount;
    private final Unit unit;

    public WallpaperAmount(BigDecimal amount, Unit unit) {
        this.amount = amount;
        this.unit = unit;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Unit getUnit() {
        return unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WallpaperAmount that = (WallpaperAmount) o;
        return getAmount().equals(that.getAmount()) &&
                getUnit() == that.getUnit();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAmount(), getUnit());
    }

    @Override
    public String toString() {
        return "amount: " + amount + ", unit=" + unit;
    }
}
