package com.pguzek.renovation.calculator.model;

import java.util.Objects;

public class Room {

    private final double length;
    private final double width;
    private final double height;
    private final Unit unit;
    private boolean isCubicShape;

    public Room(double length, double width, double height, Unit unit) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.unit = unit;
        this.isCubicShape = isCubicShape(length, width, height);
    }

    //todo this could be externalized to newly introduced factory,
    // then Room.class will be POJO, and if new room shapes will come, can be distinguished outside this class
    private boolean isCubicShape(double length, double width, double height) {
        return length == width && width == height;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public boolean isCubicShape() {
        return isCubicShape;
    }

    public Unit getUnit() {
        return unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Double.compare(room.getLength(), getLength()) == 0 &&
                Double.compare(room.getWidth(), getWidth()) == 0 &&
                Double.compare(room.getHeight(), getHeight()) == 0 &&
                isCubicShape() == room.isCubicShape() &&
                getUnit() == room.getUnit();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLength(), getWidth(), getHeight(), getUnit(), isCubicShape());
    }

    @Override
    public String toString() {
        return "Room{" +
                "length=" + length +
                ", width=" + width +
                ", height=" + height +
                ", unit=" + unit +
                '}';
    }
}
