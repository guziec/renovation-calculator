package com.pguzek.renovation.calculator.model;

import java.util.List;
import java.util.Set;

public class RenovationInfo {

    private final WallpaperAmount totalAmountWallpaperNeeded;
    private final List<Room> cubicShapedRooms;
    private final Set<Room> repeatableRooms;

    public RenovationInfo(WallpaperAmount totalAmountWallpaperNeeded,
                          List<Room> cubicShapedRooms, Set<Room> reapeatableRooms) {
        this.totalAmountWallpaperNeeded = totalAmountWallpaperNeeded;
        this.cubicShapedRooms = cubicShapedRooms;
        this.repeatableRooms = reapeatableRooms;
    }

    public WallpaperAmount getTotalAmountWallpaperNeeded() {
        return totalAmountWallpaperNeeded;
    }

    public List<Room> getCubicShapedRooms() {
        return cubicShapedRooms;
    }

    public Set<Room> getRepetableRooms() {
        return repeatableRooms;
    }
}
