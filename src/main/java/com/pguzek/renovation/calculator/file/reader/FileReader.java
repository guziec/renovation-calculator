package com.pguzek.renovation.calculator.file.reader;

import com.pguzek.renovation.calculator.file.reader.parser.LineParser;
import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class FileReader {

    private static final Logger log = LoggerFactory.getLogger(FileReader.class);
    private LineParser lineParser;

    public FileReader(LineParser lineParser) {
        this.lineParser = lineParser;
    }

    public List<Room> readFile(String filePath, Unit dimensionsUnit) {
        try {
            //todo think about different file loading/ passing path by user or via application parameter
            URL fileUrl = getClass().getClassLoader().getResource(filePath);
            Path path = Paths.get(fileUrl.toURI());
            log.debug(String.format("Reading file with input data from : %s", filePath));
            return Files.readAllLines(path, StandardCharsets.UTF_8)
                    .stream()
                    .map(line -> lineParser.parseLine(line, dimensionsUnit))
                    .collect(Collectors.toList());

        } catch (IOException | URISyntaxException e) {
            log.error(String.format("Could not read file: %s", filePath), e);
            return List.of();
        }
    }
}