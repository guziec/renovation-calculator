package com.pguzek.renovation.calculator.file.reader.parser;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;

public interface LineParser {

    Room parseLine(String line, Unit dimensionUnit);
}
