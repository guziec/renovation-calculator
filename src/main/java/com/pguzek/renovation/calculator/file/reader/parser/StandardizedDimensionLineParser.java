package com.pguzek.renovation.calculator.file.reader.parser;

import com.pguzek.renovation.calculator.model.Room;
import com.pguzek.renovation.calculator.model.Unit;

public class StandardizedDimensionLineParser implements LineParser {

    private static final String DIMENSION_SEPARATOR = "x";
    private static final int STANDARDIZED_ROOM_DIMENSIONS = 3;

    public Room parseLine(String line, Unit dimensionUnit) {
        String[] dimensions = line.split(DIMENSION_SEPARATOR);
        //todo add regexp validation
        if (dimensions.length == STANDARDIZED_ROOM_DIMENSIONS) {
            return new Room(Double.valueOf(dimensions[0]), Double.valueOf(dimensions[1]),
                    Double.valueOf(dimensions[2]), dimensionUnit);
        } else {
            throw new IllegalArgumentException(String.format("Value [%s] can not be parsed," +
                    " because doesn't contain exactly 3 dimensions," +
                    " or separator is different then expected", line));
        }
    }
}
